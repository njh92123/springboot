package com.nogoon.blog.repository;

import com.nogoon.blog.domain.Board;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
    Board findByTitle(String title);
}
