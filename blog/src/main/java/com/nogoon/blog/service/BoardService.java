package com.nogoon.blog.service;

import com.nogoon.blog.domain.Board;
import com.nogoon.blog.repository.BoardRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BoardService {

    private final BoardRepository boardRepository;

    public BoardService(BoardRepository boardRepository) {
        this.boardRepository = boardRepository;
    }

    // 글 작성
    public void write(Board board) {
        board.setCreatedDateNow();
        boardRepository.save(board);
    }

    // 글 수정
    public void update(Long boardIdx, Board board) {
        Board oldBoard = findBoardByIdx(boardIdx);
        oldBoard.update(board);
        boardRepository.save(oldBoard);
    }

    // 글 삭제
    public void delete(Long boardIdx) {
        boardRepository.deleteById(boardIdx);
    }
    
    // 글 목록 조회
    public Page<Board> findBoardList(Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber() <= 0 ? 0 : pageable.
                getPageNumber() - 1, pageable.getPageSize());
        return boardRepository.findAll(pageable);
    }

    public Board findBoardByIdx(Long boardIdx) { return boardRepository.findById(boardIdx).orElse(new Board()); }
}
