package com.nogoon.blog.controller;

import com.nogoon.blog.domain.Board;
import com.nogoon.blog.domain.Reply;
import com.nogoon.blog.service.BoardService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/board")
public class BoardController {

    private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

    @Autowired
    BoardService boardService;

    // 굴 작성 화면
    @RequestMapping(value = "/writeView", method = RequestMethod.GET)
    public String writeView() {
        logger.info("writeView");
        return "/board/writeView";
    }

    // 글 작성
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> writeBoard(@RequestBody Board board) throws Exception {
        logger.info("post");
        boardService.write(board);
        return new ResponseEntity<>("{}", HttpStatus.CREATED);
    }

    // 글 수정 화면
    @RequestMapping(value = "/updateView", method = RequestMethod.GET)
    public String updateView(@RequestParam(value = "boardIdx") Long boardIdx, Model model) {
        logger.info("updateView " + boardIdx);
        model.addAttribute("board", boardService.findBoardByIdx(boardIdx));
        return "/board/updateView";
    }

    // 글 수정
    @RequestMapping(value = "/{boardIdx}", method = RequestMethod.POST)
    public ResponseEntity<?> updateBoard(@PathVariable("boardIdx")Long boardIdx,
                                         @RequestBody Board board) {
        logger.info("update");
        boardService.update(boardIdx, board);
        return new ResponseEntity<>("{}", HttpStatus.OK);
    }

    // 글 목록 조회
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(@PageableDefault Pageable pageable, Model model) {
        logger.info("list");
        model.addAttribute("boardList", boardService.findBoardList(pageable));
        return "/board/list";
    }

    // 글 조회
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String readBoard(@RequestParam(value = "boardIdx", defaultValue = "0") Long boardIdx, Model model) {
        logger.info("read");
        model.addAttribute("board", boardService.findBoardByIdx(boardIdx));
        return "/board/read";
    }

    // 글 삭제
    @RequestMapping(value = "/{boardIdx}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBoard(@PathVariable("boardIdx")Long boardIdx) {
        logger.info("delete");
        boardService.delete(boardIdx);
        return new ResponseEntity<>("{}", HttpStatus.OK);
    }
}
